#!/usr/bin/python

# Copyright 2013 Zachary Schoenstadt
# All Rights Reserved

# Author: Zachary Schoenstadt <zls25@cs.drexel.edu>
# File: cthparse.py
# Date: 9/20/2012

import sys
from cthlex import *
import ply.yacc as yacc
import apt

#~~~~~~~~~~~~~~
# SCRIPT <- starts here
def p_script(p):
	"""script :
			  |	statement_list"""
	if len(p) == 1:
		print "Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn"
		p[0] = apt.StmtList([])
	else:
		p[0] = apt.StmtList(p[1])


#~~~~~~~~~~~~~~
# STATEMENT LIST
def p_statement_list(p):
	"""statement_list :	statement
					  | statement_list statement"""
	if len(p) == 2:
		p[0] = [p[1]]
	else:
		p[0] = p[1] + [p[2]]


#~~~~~~~~~~~~~~
# COMPOUND STATEMENT
def p_compound_statment(p):
	"""compound_statement : START END
						 | START statement_list END"""
	if len(p) == 3:
		p[0] = apt.StmtList([])
	else:
		x = apt.StmtList(p[2])
		x.scoped(True)
		p[0] = x


#~~~~~~~~~~~~~~
# EXPRESSION STATEMENT
def p_expression_statement(p):
	"""expression_statement : expression"""
	p[0] = p[1] #XXX Might need class wrapper


#~~~~~~~~~~~~~~
# SELECTION STATEMENT
def p_selection_statement(p):
	"""selection_statment : IF expression statement
						  | IF expression statement ELSE statement"""
	if len(p) == 4:
		p[0] = apt.If(p[2],p[3])
	else:
		p[0] = apt.If(p[2],p[3],p[5])
		
		
#~~~~~~~~~~~~~~
# SELECTION STATEMENT
def p_iteration_statement(p):
	"""iteration_statement : WHILE expression statement"""
	p[0] = apt.While(p[2],p[3])


#~~~~~~~~~~~~~~
# SELECTION STATEMENT
def p_jump_statement(p):
	"""jump_statement : RETURN expression"""
	p[0] = apt.Return(p[2])


#~~~~~~~~~~~~~~
# IO STATEMENT
def p_io_statement(p):
	"""io_statement : WRITE expression
					| READ IDENTIFIER"""
	if p[1] == "WRITE":
		p[0] = apt.Write(p[2])
	elif p[1] == "READ":
		p[0] = apt.Read(p[2])

#~~~~~~~~~~~~~~
# STATEMENT STATEMENT
def p_statement(p):
	"""statement : compound_statement
				 | expression_statement
				 | selection_statment
				 | iteration_statement
				 | jump_statement
				 | io_statement"""
	p[0] = p[1]


#~~~~~~~~~~~~~~
# ASSIGN EXPRESSION
def p_assign_expression(p):
	"""assign_expression : logical_or_expression
						 | ident ASSIGN assign_expression
						 | FUNC LBRACE function_parameter RBRACE statement"""
	if len(p) == 2:
		p[0] = p[1]
	elif len(p) == 4:
		p[0] = apt.Operator(p.slice[2].type,(p[1][1],p[1][0],p[3]))
	elif len(p) == 6:
		p[0] = apt.FunctionDef(p[3],p[5])

def p_function_parameter(p):
	"""function_parameter : 
			 			  | parameter_list """
	if len(p) == 1:
		p[0] = []
	else:
		p[0] = p[1]

def p_parameter_list(p):
	"""parameter_list : IDENTIFIER
					  | parameter_list COMMA IDENTIFIER"""
	if len(p) == 2:
		p[0] = [p[1]]
	else:
		p[0] = p[1] + [p[3]]


#~~~~~~~~~~~~~~
# LOGIC OR EXPRESSION
def p_logical_or_expression(p):
	"""logical_or_expression : logical_and_expression
							 | logical_or_expression LOR logical_and_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# LOGIC AND EXPRESSION
def p_logical_and_expression(p):
	"""logical_and_expression : bitwise_or_expression
							  | logical_and_expression LAND bitwise_or_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# BITWISE OR EXPRESSION
def p_bitwise_or_expression(p):
	"""bitwise_or_expression : bitwise_xor_expression
							 | bitwise_or_expression OR bitwise_xor_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# BITWISE XOR EXPRESSION
def p_bitwise_xor_expression(p):
	"""bitwise_xor_expression : bitwise_and_expression
							  | bitwise_xor_expression XOR bitwise_and_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# BITWISE AND EXPRESSION
def p_bitwise_and_expression(p):
	"""bitwise_and_expression : equality_expression
							  | bitwise_and_expression AND equality_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# EQUALITY EXPRESSION
def p_equality_expression(p):
	"""equality_expression : relational_expression
						   | equality_expression EQ relational_expression
						   | equality_expression NE relational_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# RATIONAL EXPRESSION
def p_relational_expression(p):
	"""relational_expression : shift_expression
							 | relational_expression LT shift_expression
							 | relational_expression GT shift_expression
							 | relational_expression LE shift_expression
							 | relational_expression GE shift_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# BITWISE SHIFT EXPRESSION
def p_shift_expression(p):
	"""shift_expression : additive_expression
						| shift_expression LSHIFT additive_expression
						| shift_expression RSHIFT additive_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# ADDITIVE EXPRESSION
def p_additive_expression(p):
	"""additive_expression : multiplicative_expression
						   | additive_expression PLUS multiplicative_expression
						   | additive_expression MINUS multiplicative_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# MULTIPLICATIVE EXPRESSION
def p_multiplicative_expression(p):	
	"""multiplicative_expression : power_expression 
								 | multiplicative_expression TIMES power_expression
								 | multiplicative_expression DIVIDE power_expression 
								 | multiplicative_expression MOD power_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# POWER EXPRESSION
def p_power_expression(p):
	"""power_expression	: unary_expression
						| unary_expression POWER power_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.Operator(p.slice[2].type,(p[1],p[3]))


#~~~~~~~~~~~~~~
# UNARY EXPRESSION
def p_unary_expression(p):
	"""unary_expression	: postfix_expression
						| PLUS postfix_expression
						| MINUS postfix_expression
						| NOT unary_expression
						| LNOT unary_expression"""
	if len(p) == 2:
		p[0] = p[1]
	else:
		p[0] = apt.UnaryOperator(p.slice[1].type,p[2])


#~~~~~~~~~~~~~~
# POSTFIX EXPRESSION
def p_postfix_expression(p):
	"""postfix_expression : primary_expression 
						| postfix_expression LBRACE function_argument RBRACE"""
						# postfix_expression '.' IDENTIFIER 
						# postfix_expression '[' expression ']'
	if len(p) == 2:	
		p[0] = p[1]
	else:
		p[0] = apt.FunctionCall(p[1],p[3])

def p_function_argument(p):
	"""function_argument :
						 | argument_list """
	if len(p) == 1:
		p[0] = []
	else:
		p[0] = p[1]

def p_argument_list_1(p):
	"""argument_list : assign_expression
					 | argument_list COMMA  assign_expression"""
	if len(p) == 2:
		p[0] = [p[1]]
	else:
		p[0] = p[1] + [p[3]]


#~~~~~~~~~~~~~~
# ARGUMENT LIST
def p_primary_expression(p):
	"""primary_expression : INTEGER
						| FLOAT
						| STRING_LITERAL
						| LPAREN expression RPAREN"""
	if len(p) == 2:
		p[0] = apt.Const(p[1])
	else:
		p[0] = p[2]

def p_primary_expression_1(p):
	"""primary_expression : ident"""
	p[0] = apt.Identifier(p[1][0],p[1][1])

def p_ident(p):
	"""ident : location IDENTIFIER"""
	p[0] = (p[2],p[1])	

def p_location(p):
	"""location : 
			| HERE 
			| NOTHERE"""
	if len(p) == 1:
		p[0] = ""
	else:
		p[0] = p[1]

def p_expression(p):
	"""expression :	assign_expression"""
	p[0] = p[1]

def p_error(p):
    print p

# BUILD GRAMMAR
yacc.yacc()
