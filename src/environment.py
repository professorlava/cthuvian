# Copyright 2013 Zachary Schoenstadt
# All Rights Reserved

# Author: Zachary Schoenstadt <zls25@cs.drexel.edu>
# File: environment.py
# Date: 9/20/2012

import sys

class FrameError(Exception):
	def __init__(self,e):
		self.__e__ = e
	def __str__(self):
		return repr(self.__e__)

class Environment(object):
	def __init__(self):
		self.__env__ = [{}]
		self.__depth__ = 0
		self.__return__ = None
	
	def depth(self):
		''' Frame depth '''		
		return self.__depth__

	def extend(self):
		''' Increase Scope Frame '''
		self.__env__.append({})
		self.__depth__ += 1
		
	def retract(self):
		''' Decrease Scope Frame'''
		if self.__depth__ > 0 :		
			self.__env__.pop()
			self.__depth__ -= 1
		else:
			raise FrameError('attempted to retract past frame 0')

	def get(self, identifier,frange=None):
		''' Get *identifier* from nearest symbol table, None if not defined anywhere'''
		symbol = None
		once = False
		if frange == None:
			frange = xrange(self.__depth__,-1,-1)
		for i in frange:
			once = True 
			symbol = self.__env__[i].get(identifier)
			if symbol:
				return symbol
		if not once:
			raise FrameError('attempted to retract past frame 0')
		if symbol == None:
			raise NameError(identifier+" does not exist in requested context")

	def put(self, identifier,symbol,frame=None):
		''' Put *identifier* into current frame '''
		if frame == None:
			frame = self.__depth__		
		self.__env__[frame][identifier] = symbol

	def dump(self):
		sys.stdout.write('Environment Dump\n')
		sys.stdout.flush()
		for i in xrange(self.__depth__,-1,-1):
			sys.stdout.write('Frame ' + str(i)+'\n')
			for symbol in self.__env__[i]:
				sys.stdout.write(str(symbol)+' '+str(self.__env__[i][symbol])+'\n')
			sys.stdout.flush()

