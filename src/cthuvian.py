#!/usr/bin/python

# Copyright 2013 Zachary Schoenstadt
# All Rights Reserved

# Author: Zachary Schoenstadt <zls25@cs.drexel.edu>
# File: cthuivian.py
# Date: 9/24/2012

import sys
if sys.version_info[0] >= 3:
    raw_input = input

import cthlex
import cthyacc
import apt
from environment import *


def AddBuiltin(env):
	builtin = cthyacc.yacc.parse(
	"""
	syha'h := uln {x} lw'nafh x+0
	""")
	builtin.eval(env)

if len(sys.argv) == 2:
	data = open(sys.argv[1]).read()
	progtree = cthyacc.yacc.parse(data)    
	if not progtree: raise SystemExit
	
	env = Environment()
	AddBuiltin(env)    
	#try:
	progtree.eval(env)
    #    raise SystemExit
    #except RuntimeError:
    #    pass


