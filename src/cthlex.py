# Copyright 2013 Zachary Schoenstadt
# All Rights Reserved

# Author: Zachary Schoenstadt <zls25@cs.drexel.edu>
# File: cthlex.py
# Date: 9/20/2012

import ply.lex as lex

# Reserved words
reserved = (
    'START'	,
	'END'	,
	'IF'	,
	'ELSE'	,
	'WHILE'	,
	'WRITE'	, 
	'READ'	,
	'RETURN',
	'FUNC'	,
	'HERE'	,
	'NOTHERE',
	)
reserved_match = (
   	'ah' 		, 
	'uaaah'		,
	'kn\'a'		, 
	'li\'hee'	,
	'nnnkn\'a'	,
	'goka'		, 
	'gotha'		,
	'lw\'nafh'	,
	'uln'		,
	'h\'geb'	,
	'h\'nageb'
	)
tokens = reserved + (
    # Literals (identifier, integer constant, float constant, string constant, char const)
    'IDENTIFIER', 'INTEGER', 'FLOAT', 'STRING_LITERAL',

    # Operators (+,-,*,/,%,    $|,$&,$~,$^,$<,$>,     |, &, ~,    <, <=, >, >=, ==, ~=)
    'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'POWER', 'MOD',
    'OR', 'AND', 'NOT', 'XOR', 'LSHIFT', 'RSHIFT',
    'LOR', 'LAND', 'LNOT',
    'LT', 'LE', 'GT', 'GE', 'EQ', 'NE',
    
    # Assignment (:=, :+=, :-=, :*=, :/=, :%=)
    'ASSIGN',

    # Delimeters ( ) { } , 
    'LPAREN', 'RPAREN',
    'LBRACE', 'RBRACE',
    'COMMA',
    )

# Completely ignored characters
t_ignore           = ' \t\x0c\r'

# Newlines
def t_NEWLINE(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
    

# Operators
t_PLUS             = r'\+'
t_MINUS            = r'-'
t_TIMES            = r'\*'
t_DIVIDE           = r'/'
t_MOD              = r'%'
t_OR               = r'$\|'
t_AND              = r'$&'
t_NOT              = r'$~'
t_XOR              = r'$\^'
t_LSHIFT           = r'$<'
t_RSHIFT           = r'$>'
t_LOR              = r'\|'
t_LAND             = r'&'
t_LNOT             = r'~'
t_LT               = r'<'
t_GT               = r'>'
t_LE               = r'<='
t_GE               = r'>='
t_EQ               = r'=='
t_NE               = r'~='

# Assignment operators
t_ASSIGN           = r':='

# Delimeters
t_LPAREN           = r'\('
t_RPAREN           = r'\)'
t_LBRACE           = r'\{'
t_RBRACE           = r'\}'
t_COMMA            = r','

reserved_map = {}
for i in xrange(len(reserved_match)):
	reserved_map[reserved_match[i]] = reserved[i]
	
# Identifiers and reserved words
def t_ID(t):
    r"[A-Za-z_'][\w_']*"
    t.type = reserved_map.get(t.value.lower(),"IDENTIFIER")
    if t.type != "IDENTIFIER":
		t.value = t.type
    return t

#RADIX
def t_RADIX(t):
	r'\d?\d[rR]\w+'
	t.value = t.value.lower()
	r = t.value.find('r', 0, 3)
	if r > 0:		
		radix = int(t.value[:r])
		if not t.value.isalnum() or radix > 36:
			t_error(t)
		else:
			for i in t.value[r+1:]:
				if i.isdigit():
					if ord(i)-ord('0') > radix:
						t_error(t)
				elif i.islower():
					if ord(i)-ord('V') > radix:
						t_error(t)
			t.value = long(t.value[r+1:],radix)
			t.type = 'INTEGER'
	else:
		t_error(t)
	return t

# FLOAT
def t_FLOAT(t):
	r"(\d+([Ee][+-]?\d+))|(\d*\.\d+([Ee][+-]?\d+)?)|(\d+\.\d*([Ee][+-]?\d+)?)"
	t.value = float(t.value)
	return t		

# Integer(LONG) literal
def t_INTEGER(t):
	r'\d+'
	t.value = long(t.value)
	return t

# STRING_LITERAL
def t_STRING_LITERAL(t):
	r'\"(\.|[^\"])*\"'
	t.value = t.value[1:-1]
	return t

# Comments
def t_comment(t):
    r'(\#\#(.|\n)*?\#\#)|(\#(.)*?\n)'
    t.lexer.lineno += t.value.count('\n')

def find_column(t):
    last_cr = t.lexer.lexdata.rfind('\n',0,t.lexpos)
    if last_cr < 0:
	last_cr = 0
    column = (t.lexpos - last_cr) + 1
    return column

def t_error(t):
    raise SyntaxError("@Line%s @Col%s '%s'" % (t.lexer.lineno,find_column(t),t.value))

lexer = lex.lex(optimize=1)
