# Copyright 2013 Zachary Schoenstadt
# All Rights Reserved

# Author: Zachary Schoenstadt <zls25@cs.drexel.edu>
# File: apt.py
# Date: 9/20/2012

import sys

class ReturnCall(Exception):
	def __init__(self,val):
		self.__e__ = "Invalid  call to return"
		self.val = val
	def __str__(self):
		return self.__e__

class Parsenode(object):
	def __init__(self,evalfnc):
		self.evalfn = evalfnc
		self._shiftframe = False

	def eval(self,env):
		p = self._shiftframe
		if p: 
			env.extend() 
		r = self.evalfn(self,env)
		if p:
			env.retract()
		return r

	def scoped(self,boo):
		self._shiftframe = boo


#~~~~~~~~~~~~~~~~~
# STATEMENT LIST STUFF
def StmtList_eval(self,env):
	for i in xrange(len(self.data)):		
		self.data[i].eval(env)		
		
def StmtList(lst):
	new = Parsenode(StmtList_eval)
	new.data = lst
	return new

#~~~~~~~~~~~~~~~~~
# IF 
def If_eval(self,env):
	if self.data[0].eval(env)[1]:
		self.data[1].eval(env)
	else:
		if self.data[2]:
			self.data[2].eval(env)
		
def If(cond,stmt1,stmt2=None):
	new = Parsenode(If_eval)
	new.scoped(False)
	stmt1.scoped(True)
	if stmt2 != None:
		stmt2.scoped(True)
	new.data = (cond,stmt1,stmt2)
	return new

#~~~~~~~~~~~~~~~~~~
# WHILE
def While_eval(self,env):
	while self.data[0].eval(env)[1]:
		self.data[1].eval(env)

def While(cond,stmt):
	new = Parsenode(While_eval)
	new._shiftframe = False
	stmt.scoped(True)
	new.data = (cond,stmt)
	return new

#~~~~~~~~~~~~~~~~~~
# FUNCTION & RETURN
def Return_eval(self,env):
	raise ReturnCall(self.data.eval(env)) #if caught, doesnt interupt or print or anything
								#FunctionCall will catch, otherwise will cause runtime error
def Return(expr):
	new = Parsenode(Return_eval)
	new.data = expr
	return new

def Funcdef_eval(self,env):
	return ['FUNCTION',self.data]

def FunctionDef(param,stmt):
	new = Parsenode(Funcdef_eval)
	new.data = (param,stmt)
	return new

def Funccall_eval(self,env):
	fn = self.data[0].eval(env)
	if not fn or fn[0] != 'FUNCTION':
		raise NameError(self.data[0] + " is not a function")
	#XXX make sure arg and param size line ups	
	syms = []
	for i in xrange(len(fn[1][0])):
		syms.append(self.data[1][i].eval(env))
	depth_record = env.depth()
	env.extend()
	for i in xrange(len(fn[1][0])):
		env.put(fn[1][0][i],syms[i])	
	try:	
		fn[1][1].scoped(False)
		fn[1][1].eval(env)
		env.retract()
	except ReturnCall as r:		
		while env.depth() > depth_record:
			env.retract()
		return r.val

def FunctionCall(fnname,args):
	new = Parsenode(Funccall_eval)
	new.data = (fnname,args)
	return new

#def WrapFunction(env,function,newname,paramnum,returntype):
#	env.put(newname,('BOUNDFUNC',(function,paramnum,returntype)))
	#from funccall_eval	
	#if fn[0] == 'BOUNDFUNC':
	#	syms = []
	#	for i in xrange(fn[1][1]):
	#		syms.append(self.data[1][i].eval(env)[1])
	#	return (fn[1][2],fn[1][0](*syms))


#~~~~~~~~~~~~~~~~
# IO
def Write_eval(self,env):
	sys.stdout.write(str(self.data[0].eval(env)[1]))
	sys.stdout.flush()
	
def Write(expr):
	new = Parsenode(Write_eval)
	new.data = (expr,)
	return new
	
def Read_eval(self,env):
	s = sys.stdin.readline()[:-1]
	env.put(self.data[0],('STRING',s))

def Read(expr):
	new = Parsenode(Read_eval)
	new.data = (expr,)
	return new

#~~~~~~~~~~~~~~~~
# ALL OPERATIONS
def typecheck(p):
	pass

def assign_eval(self,env):
	# tuple ( loc, IDENTIFIER, expression)
	val = None	
	if self.data[0] == 'HERE':
		val = self.data[2].eval(env)	
		env.put(self.data[1],val,env.depth())
	elif self.data[0] == 'NOTHERE': #XXX Potential fun, expand to variable depth in grammar
		symbol = None 
		symbol = env.get(self.data[1],xrange(env.depth()-1,-1,-1))
		val = self.data[2].eval(env)
		symbol[0] = val[0]
		symbol[1] = val[1]
	else:
		symbol = None 
		try:
			symbol = env.get(self.data[1])
			val = self.data[2].eval(env)
			symbol[0] = val[0]
			symbol[1] = val[1]
		except NameError:
			val = self.data[2].eval(env)
			env.put(self.data[1],val)
	return val
	
def lor_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typecheck(A,B)
	if A[1] or B[1]:
		return ['INTEGER',1]
	else:
		return ['INTEGER',0]	
	
def land_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typecheck(A,B)
	if A[1] and B[1]:
		return ['INTEGER',1]
	else:
		return ['INTEGER',0]	

def or_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] | B[1]
	return ['INTEGER', r]

def xor_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] ^ B[1]
	return ['INTEGER', r]

def and_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] & B[1]
	return ['INTEGER', r]

def eq_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typecheck(A,B)
	if A[1] == B[1]:
		return ['INTEGER',1]
	else:
		return ['INTEGER',0]

def ne_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typecheck(A,B)
	if A[1] != B[1]:
		return ['INTEGER',1]
	else:
		return ['INTEGER',0]

def lt_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typecheck(A,B)
	if A[1] < B[1]:
		return ['INTEGER',1]
	else:
		return ['INTEGER',0]

def gt_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typecheck(A,B)
	if A[1] > B[1]:
		return ['INTEGER',1]
	else:
		return ['INTEGER',0]

def le_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typecheck(A,B)
	if A[1] <= B[1]:
		return ('INTEGER',1)
	else:
		return ('INTEGER',0)

def ge_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typecheck(A,B)
	if A[1] >= B[1]:
		return ['INTEGER',1]
	else:
		return ['INTEGER',0]

def lshift_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] << B[1]
	return ['INTEGER', r]

def rshift_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] >> B[1]
	return ['INTEGER', r]

def plus_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] + B[1]
	return ['INTEGER', r]

def minus_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] - B[1]
	return ['INTEGER', r]

def times_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] * B[1]
	return ['INTEGER', r]

def divide_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] / B[1]
	return ['INTEGER', r]

def mod_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#XXX wrap exception
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] % B[1]
	return ['INTEGER', r]

def power_eval(self,env):
	A = self.data[0].eval(env)
	B = self.data[1].eval(env)
	#typ = typecheck(A,B)
	#XXX wrap exception
	r = A[1] ** B[1]
	return ['INTEGER', r]

ops = {
	'ASSIGN' 	: assign_eval ,
	'LOR'		: lor_eval ,
	'LAND'		: land_eval ,
	'OR'		: or_eval ,
	'XOR'		: xor_eval ,
	'AND'		: and_eval ,
	'EQ'		: eq_eval ,
	'NE'		: ne_eval ,
	'LT'		: lt_eval ,
	'GT'		: gt_eval ,
	'LE'		: le_eval ,
	'GE'		: ge_eval , 
	'LSHIFT'	: lshift_eval ,
	'RSHIFT'	: rshift_eval ,
	'PLUS'		: plus_eval ,
	'MINUS'		: minus_eval ,
	'TIMES'		: times_eval,
	'DIVIDE'	: divide_eval ,
	'MOD'		: mod_eval ,
	'POWER'		: power_eval ,
}

def Operator(op,tupl):
	new = Parsenode( ops[op] )
	new.data = tupl
	return new

def uplus_eval(self,env):
	pass
#	A = self.data[0].eval(env)
#	return A

def uminus_eval(self,env):
	A = self.data[0].eval(env)
	A[1] = -A[1]		
	if self.data[0].evalfn == Const_eval:
		# operation is on a Const, transform into Const to remains static
		# so it doesnt re-evaluate		
		self.evalfn = self.data[0].evalfn	
		self._shiftframe = self.data[0]._shiftframe
		self.data = self.data[0].data
		return self.eval(env)
	else:
		return A
				
def unot_eval(self,env):
	pass
#	A = self.data[0].eval(env)
#	A[1] = ~A[1]		
#	if self.data[0].evalfn == Const_eval:
#		# operation is on a Const, transform into Const to remains static
#		# so it doesnt re-evaluate		
#		self.evalfn = self.data[0].evalfn	
#		self._shiftframe = self.data[0]._shiftframe
#		self.data = self.data[0].data
#		return self.eval(env)
#	else:
#		return A

def ulnot_eval(self,env):
	pass
#	A = self.data[0].eval(env)
#	A[1] = not A[1]		
#	if self.data[0].evalfn == Const_eval:
#		# operation is on a Const, transform into Const to remains static
#		# so it doesnt re-evaluate		
#		self.evalfn = self.data[0].evalfn	
#		self._shiftframe = self.data[0]._shiftframe
#		self.data = self.data[0].data
#		return self.eval(env)
#	else:
#		return A

uops = {
	'PLUS'		: uplus_eval ,
	'MINUS'	: uminus_eval ,
	'NOT'		: unot_eval ,
	'LNOT'		: ulnot_eval 
}

def UnaryOperator(op,expr):
	new = Parsenode( uops[op] )
	new.data = (expr,)
	return new

def Const_eval(self,env):
	#return list(self.data)
	return self.data #had cool sideeffects

def Const(value):
	new = Parsenode(Const_eval)
	if type(value) == str:
		new.data = ['STRING',value.decode('string-escape')]
	elif type(value) == long:
		new.data = ['INTEGER',value]
	elif type(value) == float:
		new.data = ['FLOAT',value]
	else:
		raise NameError('no identifiable type')
	return new

def Ident_eval(self,env):
	symbol = None
	if self.data[1] == 'HERE':
		symbol = env.get(self.data[0],[env.depth()])
	elif self.data[1] == 'NOTHERE':
		symbol = env.get(self.data[0],xrange(env.depth()-1,-1,-1))
	else:
		symbol = env.get(self.data[0])
	return symbol

def Identifier(name,loc):
	new = Parsenode(Ident_eval)
	new.data = (name, loc)
	return new

def Bind():
	pass
